# Import required modules
import numpy as np
import matplotlib.pyplot as plt
import scipy.fftpack
import ipywidgets as wid

# Define plot attributes
plt.rcParams['figure.figsize'] = (16, 10) # Does not seem to work on Azure for some reason
plt.rcParams['font.size'] = 16
plt.rcParams['axes.spines.top'] = False
plt.rcParams['axes.spines.right'] = False
plt.rcParams['axes.labelsize'] = 'large'
plt.rcParams['xtick.labelsize'] = 'small'
plt.rcParams['ytick.labelsize'] = 'small'

"""
Compute the square wave function

p0 : Amplitude of the function
T  : Period of the function
t  : Domain at which to evaluate the function
"""
def square_wave(p0, T, t):
    return p0*np.sign(np.sin(2*np.pi*t/T))

"""
Compute the sawtooth wave function

p0 : Amplitude of the function
T  : Period of the function
t  : Domain at which to evaluate the function
"""
def sawtooth_wave(p0, T, t):
        return 2*p0*(t/T - np.floor(t/T + 1/2))

"""
Compute and plot the Fourier transform of a given load function and the SDOF response under it

p0       : Amplitude of the load function
k        : Stiffness of the SDOF system
p_name   : Type of the periodic load function to use
T        : Period of the load function
T_n      : Natural period of the SDOF system
zeta     : Damping ratio of the SDOF system
num_harm : Number of harmonics to include (must be lesser than half the number of elements in t)
"""
def compute_plot_fourier_transform_response(p0, k, p_name, T, T_n, zeta, num_harm):
    
    # Compute the load function over one period
    num_pts = 512
    t = np.linspace(0, T, num_pts)
    func_map = {'Square wave':square_wave, 'Sawtooth wave':sawtooth_wave}
    p = func_map[p_name](p0, T, t)
    
    # Compute the plotting domain and plot the load function
    tplot = []
    pplot = []
    t_max = 4
    num_rep = int(np.ceil(t_max/T))
    for i in range(num_rep):
        tplot += [t + i*t[-1]]
        pplot += [p]
    tplot = np.hstack(tplot)
    pplot = np.hstack(pplot)
    
    plt.figure(figsize=(16, 10))
    ax1 = plt.subplot(211)
    plt.plot(tplot, pplot, lw=2, color='k')

    # Compute the fourier transform of the load function
    P = scipy.fftpack.fft(p)/num_pts
    
    # Plot the contant component of the load function and the response to it
    p_fourier = np.real(P[0])*np.ones(num_rep*num_pts)
    ax1.plot(tplot, p_fourier, lw=1, color='0.7')

    omega_n = 2*np.pi/T_n
    u_p_fourier = np.real(P[0])/k*np.ones(num_rep*num_pts)
    ax2 = plt.subplot(212)
    ax2.plot(tplot, u_p_fourier, lw=1, color='0.7')
    
    # Plot the first n harmonic components of the load function and the response to it
    a = 2*np.real(P)[1:num_harm + 1]
    b = -2*np.imag(P)[1:num_harm + 1]
    omega = 2*np.pi/T
    for i in range(1, num_harm + 1):
        p_harmonic = a[i - 1]*np.cos(i*omega*tplot) + b[i - 1]*np.sin(i*omega*tplot)
        ax1.plot(tplot, p_harmonic, lw=1, color='0.7')
        p_fourier += p_harmonic

        i_omega = i*omega
        i_beta = i_omega/omega_n
        if zeta == 0 and i_beta == 1:
            u_p_harmonic = a[i - 1]/2/k*omega_n*tplot*np.sin(omega_n*tplot) - \
                    b[i - 1]/2/k*omega_n*tplot*np.cos(omega_n*tplot)
        else:
            u_p_harmonic = a[i - 1]/k*((1 - i_beta**2)*np.cos(i_omega*tplot) + \
                    2*zeta*i_beta*np.sin(i_omega*tplot))/((1 - i_beta**2)**2 + (2*zeta*i_beta)**2) + \
                    b[i - 1]/k*((1 - i_beta**2)*np.sin(i_omega*tplot) - \
                    2*zeta*i_beta*np.cos(i_omega*tplot))/((1 - i_beta**2)**2 + (2*zeta*i_beta)**2)
        ax2.plot(tplot, u_p_harmonic, lw=1, color='0.7')
        u_p_fourier += u_p_harmonic
    
    # Plot the fourier transforms of the load function and the response to it
    ax1.plot(tplot, p_fourier, lw=2, color='#BF1C1C')
    ax2.plot(tplot, u_p_fourier, lw=2, color='#BF1C1C')
    
    # Finalise plot
    ax1.grid(False)
    ax1.set_xlim(tplot[0], t_max)
    ax1.set_ylabel('$p(t)/p_0$')

    ax2.grid(False)
    ax2.set_xlim(tplot[0], t_max)
    ax2.set_xlabel('$t$')
    ax2.set_ylabel('$u_p(t)/(u_{st})_0$')
    
    plt.tight_layout(pad=0.5)
    plt.show()
