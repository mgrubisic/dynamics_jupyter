import numpy as np
RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoNEF=False
RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikBoFE=True
RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikBoFN=len
RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikBoEF=int
RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikBoEN=range
RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoNEB=np.pi
RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoNFE=np.random
RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoNFB=np.exp
RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoNBE=np.meshgrid
RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoNBF=np.arange
RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoENF=np.zeros
import matplotlib.pyplot as plt
import scipy.stats,scipy.interpolate
import ipywidgets as wid
plt.rcParams['figure.figsize']=(16,10)
plt.rcParams['font.size']=16
plt.rcParams['axes.spines.top']=RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoNEF
plt.rcParams['axes.spines.right']=RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoNEF
plt.rcParams['axes.labelsize']='large'
plt.rcParams['xtick.labelsize']='small'
plt.rcParams['ytick.labelsize']='small'
def random_load(alpha):
 RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoBFE=0.05
 RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoBFN=401
 RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoBEF=RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoENF(RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoBFN)
 t=RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoBFE*RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoNBF(RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoBFN)
 t1,t2=RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoNBE(t,t)
 RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoBEN=RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoNFB(-10**alpha*(t1-t2)**2)
 RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoNFE.seed(0)
 p=scipy.stats.multivariate_normal(mean=RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoBEF,cov=RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoBEN,allow_singular=RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikBoFE).rvs()
 return p,RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoBFE
"""
Compute and plot the response of a damped SDOF system to the random load
algo  : Numerical integration algorithm to use
T_n   : Natural period of the system
zeta  : Damping ratio of the system
dt    : Time step of the load
alpha : Parameter controlling the rate of variation in the load
"""
def compute_plot_response(algo,T_n,zeta,dt,alpha):
 RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoBNF=2*RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoNEB/T_n
 m=1
 c=2*zeta*RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoBNF*m
 k=m*RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoBNF**2
 RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoBNE,RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoBFE=random_load(alpha)
 RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoBFN=RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikBoFN(RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoBNE)
 RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFBE=RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoBFE*RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoNBF(RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoBFN)
 RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFBN=RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikBoEF(RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFBE[-1]/dt)
 t=dt*RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoNBF(RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFBN)
 p=scipy.interpolate.interp1d(RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFBE,RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoBNE)(t)
 if algo=='Central difference':
  u=RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoENF(RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFBN+1)
  v=0
  a=(p[0]-c*v-k*u[1])/m
  u[0]=u[1]-v*dt+a*dt**2/2
  RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFEB=m/dt**2+c/2/dt
  a1=m/dt**2-c/2/dt
  a2=k-2*m/dt**2
  for i in RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikBoEN(0,RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFBN-1):
   RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFEN=p[i]-a1*u[i]-a2*u[i+1]
   u[i+2]=RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFEN/RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFEB
  u=u[1:]
 elif algo=='Newmark avg acc':
  u=RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoENF(RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFBN)
  v=0
  a=(p[0]-c*v-k*u[0])/m
  RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFNB=0.5
  RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFNE=0.25
  a1=m/RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFNE/dt**2+c*RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFNB/RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFNE/dt
  a2=m/RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFNE/dt+c*(RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFNB/RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFNE-1)
  a3=m*(1/2/RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFNE-1)+c*dt*(RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFNB/2/RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFNE-1)
  RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFEB=k+a1
  for i in RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikBoEN(0,RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFBN-1):
   RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFEN=p[i+1]+a1*u[i]+a2*v+a3*a
   u[i+1]=RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFEN/RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFEB
   RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoEBF=(u[i+1]-u[i])*RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFNB/RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFNE/dt+v*(1-RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFNB/RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFNE)+a*dt*(1-RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFNB/2/RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFNE)
   RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoEBN=(u[i+1]-u[i])/RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFNE/dt**2-v/RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFNE/dt-a*(1/2/RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFNE-1)
   v=RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoEBF
   a=RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoEBN
 plt.figure(figsize=(16,10))
 plt.subplot(211)
 plt.plot(RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFBE,RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoBNE,color='k',lw=2)
 plt.grid(RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikBoFE)
 plt.xlim(0,RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFBE[-1])
 plt.gca().xaxis.set_ticklabels(())
 plt.ylabel(r'$p(t)$')
 plt.subplot(212)
 plt.plot(t,u,color='#BF1C1C',lw=2)
 plt.grid(RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikBoFE)
 plt.xlim(0,RrlVqdmjxbCUXMQyTuvWLtKcAIpYDnJhOGPgseSwzHafikoFBE[-1])
 plt.xlabel(r'$t$')
 plt.ylabel(r'$u(t)$')
 plt.tight_layout(pad=0.5)
 plt.show()
# Created by pyminifier (https://github.com/liftoff/pyminifier)
